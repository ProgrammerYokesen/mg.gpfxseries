<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Session;
use cookie;
use Mailgun\Mailgun;
use App\User;
use Illuminate\Support\Str;
use Validator;
use DB;

require '../vendor/autoload.php';


class mailController extends Controller
{
  /**
   * First change your credentials on .env
   * @from fill with your email
   * @subject fill with your email's subject
   * @template fill with your template's name on mailgun dashboard
   * @var string
   */
  private $emailAccount = '';
  private $from = 'GPFX Series <support@gpfxseries.com>';
  private $subject = 'Reset Password';
  private $template = 'gpfxseries-reset-password';
  private $url = 'https://gpfxseries.com/reset-password?token=';

  /**
   * Email reset password
   */
  public function sendMail(Request $req)
  {
    $this->emailAccount = $req->email;

    if ($this->validateMail() == 'true') {
      $token = $this->createUserToken();
      /**Send mail disini */
      $mg = Mailgun::create(env('MAILGUN_SECRET'), 'https://api.mailgun.net'); // For EU servers
      $params = array(
        'from'    => $this->from,
        'to'      => $this->emailAccount,
        'subject' => $this->subject,
        'template'    => $this->template,
        'v:token'   => $token,
        'h:X-Mailgun-Variables'    => '{"$token": "' . $token . '"}'
      );
      $result = $mg->messages()->send(ENV('MAILGUN_DOMAIN'), $params);
      return response()->json([
        'Status' => "Success",
        'Data' => $result
      ]);
    } else {
      /**Mail cannot be validated */
      return response()->json([
        'Status' => "Failed",
        'Message' => 'Alamat email gagal di validasi'
      ]);
    }
  }

  public function forgotPassword(Request $request)
  {
    $code = $request->token;
    $client = new Client();
    $result = $client->get(env('API_BASE_URL') . 'get_lost_password_by_code?code=' . $code, [
      'headers' => [
        'X-Authorization-Token' => md5(env('API_KEY')),
        'X-Authorization-Time'  => time()
      ]
    ]);

    $res = json_decode($result->getBody());
    $this->emailAccount = $res->email;
    $email = $res->email;

    $client = new Client();
    $user_get = $client->get(env('API_BASE_URL') . 'search_user_by_email?email=' . $email, [
      'headers' => [
        'X-Authorization-Token' => md5(env('API_KEY')),
        'X-Authorization-Time'  => time()
      ]
    ]);

    $user = json_decode($user_get->getBody());
    $name = $user->name;

    /**
     * seeing the detail where row=$id
     */
    $rules = array(
      'password' => 'required|confirmed:password_confirmation'
    );

    $validator = \Validator::make($request->all(), $rules);
    if ($validator->fails()) {
      return redirect()->route('profileIndex')->withInput()->withErrors($validator);
    } else {
      $client = new Client();
      $change_password = $client->post(env('API_BASE_URL') . 'change_password', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ],
        'form_params' => [
          'id' => $user->id,
          'email' => $user->email,
          'password' => $request->password
        ]
      ]);
      $change = json_decode($change_password->getBody());
      return response()->json([
        'Status' => 'Success',
        'Message' => 'Your password has been changed'
      ]);
    }
  }

  /**
   * Check email using mailgun
   * Exist or not
   * @return void
   */
  public function validateMail()
  {
    $client = new Client();
    $result = $client->get('https://api.mailgun.net/v3/address/validate?address=' . $this->emailAccount . '&api_key=' . env('MAILGUN_PUBLIC_KEY') . '&mailbox_verification=true');
    $res = json_decode($result->getBody());
    $check = $res->mailbox_verification;
    return $check;
  }

  public function createUserToken()
  {
    $client = new Client();
    $result = $client->get(env('API_BASE_URL') . 'search_user_by_email?email=' . $this->emailAccount, [
      'headers' => [
        'X-Authorization-Token' => md5(env('API_KEY')),
        'X-Authorization-Time'  => time()
      ]
    ]);
    $res = json_decode($result->getBody());
    $name = $res->name;
    $code = md5(date('Y-m-d H:i:s') . $name . $this->emailAccount) . time();

    $create_lost_password = $client->post(env('API_BASE_URL') . 'create_lost_password_code', [
      'headers' => [
        'X-Authorization-Token' => md5(env('API_KEY')),
        'X-Authorization-Time'  => time()
      ],
      'form_params' => [
        'email' => $this->emailAccount,
        'code' => $code,
        'created_at' => date('Y-m-d H:i:s'),
        'expired_at' => date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . '+3days'))
      ]
    ]);
    return $code;
  }

  public function blastEmail()
  {
    $emailList = \DB::table('email_list')->where('verify', 1)->get();
    $this->template = "welcome-email";
    $this->subject = "Welcome to GPFX Series";
    foreach ($emailList as $mail) {
      $this->emailAccount = $mail->email;
      $code = $mail->uuid;
      $var = '{"$name": "' . $mail->name . '","$code": "' . $code . '"}';
      if ($this->validateMail() == 'true') {
        /**Send mail disini */
        $mg = Mailgun::create(env('MAILGUN_SECRET'), 'https://api.mailgun.net'); // For EU servers
        $params = array(
          'from'    => $this->from,
          'to'      => $this->emailAccount,
          'subject' => $this->subject,
          'template'    => $this->template,
          'v:code'   => '.$code.',
          'v:name' => $mail->name,
          'h:X-Mailgun-Variables'    => $var
        );
        $result = $mg->messages()->send(ENV('MAILGUN_DOMAIN'), $params);
      }
    }
    return response()->json([
      'Status' => "Success",
      'Data' => $result
    ]);
  }

  public function sendEmail()
  {
    date_default_timezone_set("Asia/Jakarta");
    $data = DB::table('email_queues')->join('email_templates', 'email_templates.id', 'email_queues.email_template_id')
    ->join('users', 'users.email','email_queues.email_recipient')
    ->where('email_queues.is_sent','!=', 1)
    ->orWhere(function($query){
        $query->where('email_queues.type', '!=', 'onetime')->where('is_stop', 0);
    })
    ->select('email_queues.id as id', 'email_queues.email_recipient as email','email_templates.subject as subject', 'users.name as name', 'email_queues.type as type','email_templates.name as template', 'users.uuid as uuid', 'email_queues.send_at as send_at', 'email_queues.flag as flag')
    ->get();
    dd($data);
    foreach($data as $dt){
      $this->subject = $dt->subject;
      $this->template = $dt->template;
      $this->emailAccount = $dt->email;
      $this->subject = $dt->subject;
      $name = $dt->name;
      $code = $dt->uuid;
      $var = '{"$name": "' . $name . '","$code": "' . $code . '"}';
      /** Validate email, exist or not */
      if ($this->validateMail() == 'true') {

        /** Send onetime email */
        if($dt->type == "onetime"){
          $sent_at = strtotime($dt->send_at);
          $now = strtotime("now");
          if($sent_at <= $now){
            /**Send mail disini */
            $mg = Mailgun::create(env('MAILGUN_SECRET'), 'https://api.mailgun.net'); // For EU servers
            $params = array(
              'from'    => $this->from,
              'to'      => $this->emailAccount,
              'subject' => $this->subject,
              'template'    => $this->template,
              'v:code'   => '.$code.',
              'v:name' => $name,
              'h:X-Mailgun-Variables'    => $var
            );
            $result = $mg->messages()->send(ENV('MAILGUN_DOMAIN'), $params);
            $flag= $dt->flag +1;
            DB::table('email_queues')->where('id', $dt->id)->update([
                'is_sent' => 1,
                'flag' => $flag
            ]);
            echo "Success send email to ". $dt->email. "<br>";
          }
        }
        /**End send onetime */

        /**Send weekly email */
        if($dt->type == "weekly"){
          $send_at = strtotime($dt->send_at);
          $send_at_day = date("D", $send_at);
          $send_at_time = strtotime(date("H:i:s", $send_at));
          $now_day = date("D");
          $now_time = strtotime(date("H:i:s"));
          if($send_at_day == $now_day && $send_at_time <= $now_time){
            /**Send mail disini */
            $mg = Mailgun::create(env('MAILGUN_SECRET'), 'https://api.mailgun.net'); // For EU servers
            $params = array(
              'from'    => $this->from,
              'to'      => $this->emailAccount,
              'subject' => $this->subject,
              'template'    => $this->template,
              'v:code'   => '.$code.',
              'v:name' => $name,
              'h:X-Mailgun-Variables'    => $var
            );
            $result = $mg->messages()->send(ENV('MAILGUN_DOMAIN'), $params);
            $flag= $dt->flag +1;
            DB::table('email_queues')->where('id', $dt->id)->update([
                'is_sent' => 1,
                'flag' => $flag
            ]);
            echo "Success send email to ". $dt->email. "<br>";
          }
        }
        /**End send weekly email */
      }
      /**End Validate email */
    }
  }

}
