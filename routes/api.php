<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::prefix('v1')->group(function () {
    Route::post('send-email','mailController@sendMail')->name('sendEmail');
    Route::post('forgot-password','mailController@forgotPassword')->name('forgotPassword');
    Route::post('email-blast','mailController@blastEmail')->name('emailBlast');
    /**
     * Route email blast yang baru
     */
    Route::get('mail-blast','mailController@sendEmail')->name('mailBlast');
});
